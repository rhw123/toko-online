<?php 

$resultrow = tampildataadmin("SELECT * FROM produk
   INNER JOIN kategori
    ON produk.id_kategori = kategori.id_kategori");

 ?>

<div class="container mt-5" id = "kontainer">
	<div class="row">
	<?php foreach($resultrow as $rw) : ?>
		<div class="col-lg-4 pt-4">
			<div class="card">
			 <img class="card-img-top" src="gambar/<?php echo $rw['gambar'] ?>" alt="Card image cap">
		        <div class="card-body">
		          <p class="card-text"><?php echo $rw['nama_produk'] ?></p>
		          <p><?php echo number_format($rw['harga_produk']) ?></p>
		          <a href="" class="btn btn-warning btn-sm">Beli</a>
		          <a href="" class="btn btn-info btn-sm">Kembali</a>
		        </div>
		      </div>
		</div>
	<?php endforeach; ?>


	</div>
</div>