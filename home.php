<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <!-- <h1 class="display-4">Fluid jumbotron</h1>
    <p class="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p> -->
  </div>
</div>

<div class="container-fluid" id="produk">
  <div class="row">
    <div class="col-12">
      <h3 class="text-center">Produk Yang Kami Jual</h3>
    </div>
  </div>
  <div class="row mt-3" id = "row">
    <div class="col-lg-4">
     
        <img class="card-img-top" src="images/handphone.jpg" alt="Card image cap">
        <div class="card-body">
          <p class="card-text">Hp Merek Terbaru Di indonesia.</p>
        </div>
      
    </div>

    <div class="col-lg-4">
      
        <img class="card-img-top" src="images/laptop.jpeg" alt="Card image cap">
        <div class="card-body">
          <p class="card-text">Laptop Merek Terbaru Di Indonesia</p>
        </div>
     
    </div>

    <div class="col-lg-4">
      
        <img class="card-img-top" src="images/kemeja.jpg" alt="Card image cap">
        <div class="card-body">
          <p class="card-text">Baju Kemeja Terbaru Di Indonesia.</p>
        </div>
     
    </div>
  </div>
</div>

<div class="container-fluid " id="tentang">
  <div class="row">
    <div class="col-lg-12 ">
      <h4 class = "text-center">Produk Kami Sangat Terjangkau Dan Murah Harga nya <br>  Jangan Sampai Ketinggalan ya</h4>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 text-center mt-4">
     <a href="index.php?halaman=toko" class="btn btn-primary">Belanja Sekarang</a>
    </div>
  </div>
</div>