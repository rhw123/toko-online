<?php 

  require_once '../proses/proses.php';
  $resultrow = tampildataadmin("SELECT * FROM produk
   INNER JOIN kategori
    ON produk.id_kategori = kategori.id_kategori");

 ?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>AdminLTE 3 | Blank Page</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      
    </ul>

    <!-- Right navbar links -->
    
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
      <img src="../../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="../../dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Alexander Pierce</a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        
          <li class="nav-item">
            <a href="dashboard.php" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
               Produk
              
              </p>
            </a>
          </li>


          <li class="nav-item">
            <a href="../widgets.html" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
               Kategori
              
              </p>
            </a>
          </li>
          
            </ul>
          </li>
         
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          
        
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <?php if (isset($_GET['pesan'])) { ?>
        
        <div class="alert alert-primary alert-dismissible fade show" role="alert">
          <strong><?php echo $_GET['pesan']; ?></strong> 
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

      <?php } ?>
      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Daftar Produk</h3>

          <div class="card-tools">
             <button type="button" class="btn btn-primary btn-sm mr-4" data-toggle="modal" data-target="#exampleModal">Tambah Data</button>
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Produk</th>
                <th>Nama Kategori</th>
                <th>Harga Produk</th>
                <th>Deskripsi</th>
                <th>Gambar</th>
                <th>Action</th>

              </tr>
            </thead>

            <tbody>
          <?php $no = 1; ?>
          <?php foreach($resultrow as $rw) : ?>
              <tr>
                <td><?php echo $no++ ?></td>
                <td><?php echo $rw['nama_produk']; ?></td>
                <td><?php echo $rw['nama_kategori']  ?></td>
                <td>Rp. <?php echo number_format($rw['harga_produk']) ?></td>
                <td><?php echo $rw['deskripsi']; ?></td>
                <td><?php echo $rw['gambar']; ?></td>
                <td>
                  <a href="hapus.php?id=<?php echo $rw['id_produk']; ?>" class="btn btn-danger btn-sm">Hapus</a>
                  <a href="" class="btn btn-warning btn-sm">Edit</a>
                </td>
              </tr>
          <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
         List Produk
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.1.0
    </div>
    <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
</body>
</html>


<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Silakan Tambahkan Produk</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form action="" method="POST" enctype="multipart/form-data">
              <div class="form-group">
                <label for="exampleInputEmail1">Nama Produk</label>
                <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="nama_produk">
              </div>
            <?php $ambil = tampildataadmin("SELECT * FROM kategori"); ?>

              <select class="form-control" name = "id_kategori">
                <option>---</option>
                <?php foreach($ambil as $am) : ?>
                <option value="<?php echo $am['id_kategori'] ?>"><?php echo $am['nama_kategori']; ?></option>
                <?php endforeach; ?>
              </select>

              <div class="form-group">
                <label for="exampleInputPassword1">Harga Produk</label>
                <input type="text" class="form-control" id="exampleInputPassword1" name = "harga_produk">
              </div>

              <div class="form-group">
                <label for="exampleInputPassword1">Deskripsi</label>
                <input type="text" class="form-control" id="exampleInputPassword1" name = "deskripsi">
              </div>

             <div class="form-group">
              <label for="exampleFormControlFile1">Silakan Input File Gambar</label>
              <input type="file" class="form-control-file" name = "gambar" id="exampleFormControlFile1">
            </div>
              
              <button type="submit" class="btn btn-primary" name = "simpan">Simpan Data</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
           </form>
      </div>
    </div>
  </div>
</div>

<?php 

if (isset($_POST['simpan'])) {
    
    simpandata();
}

 ?>