<?php 

require_once 'proses/proses.php';

 ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Selamat Datang Di Toko Online</title>
	<link rel="stylesheet" type="text/css" href="dist/css/adminlte.min.css">
	<link rel="stylesheet" type="text/css" href="dist/css/mycss.css">
</head>
<body>


	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <div class="container">
	  	<a class="navbar-brand" href="#">Toko Saya</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
		    <div class="navbar-nav ml-auto">
		      <a class="nav-link active" href="index.php">Home <span class="sr-only">(current)</span></a>
		      <a class="nav-link" href="index.php?halaman=toko">Toko</a>
		      
		    </div>
		  </div>
	  </div>
	</nav>

	<div class="wrapper">
		<?php if (isset($_GET['halaman']))
		{
			if ($_GET['halaman'] == 'toko')
			{
				require_once 'toko.php';
			}
		}
		else
		{
			require_once 'home.php';
		}


		 ?>
	</div>


</body>
</html>